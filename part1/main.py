from datetime import datetime

def toplevel(logfile):
    def log_datetime(func):
        """Log the date and time of a function"""

        def logger(*args, **kwargs):
            with open(logfile, "a") as f:
                f.write("\n")
                f.write(f'Function: {func.__name__}\nRunning on: {datetime.today().strftime("%Y-%m-%d %H:%M:%S")}\n')
                value = func(*args, **kwargs)
                f.write(f'Function: {func.__name__}\nDone running at: {datetime.today().strftime("%Y-%m-%d %H:%M:%S")}\n')
                f.write(f'{"-"*30}') 
            return value  
        return logger
    return log_datetime


@toplevel("part1/log_fn.log")
def daily_backup():

    print("Daily backup job has finished.")





daily_backup()